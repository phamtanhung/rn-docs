# Hướng dẫn cho người bắt đầu với React Native
Trong bài viết này, mình sẽ hướng dẫn cho các bạn cách làm thế nào để tự mình bắt đầu phát triển một ứng dụng di động với framework React Native.

## React native là gì?
Vậy React Native là gì, giúp chúng ta được gì!? React Native là một nền tảng phát triển ứng dụng di động dựa vào Javascript và ReactJS. Đơn giản là vậy. Nó được thiết kế giống với ReactJS và cho phép bạn tạo ra các giao diện người dùng sử dụng declarative components. (Bạn có thể tìm hiểu thêm declarative programming ở đây: https://en.wikipedia.org/wiki/Declarative_programming).

React Native được phát triển bởi Facebook năm 2015 và nhận được rất nhiều sự quan tâm và đóng góp của cộng đồng Javascript. Hiện nay, framwork React Native đã và đang được tin tưởng bởi những công ty lớn phát triển các sản phẩm của họ như: Skype, Pinteres, Instagram, Tesla, Walmart, Baidu, SoundClound, Uber, vv...


### Tại sao phải dùng React Native!?
Có rất nhiều cuộc tranh luận diễn ra và có nhiều lí do tại sao chúng ta phải cần đến framework này. 
- Thứ nhất, React Native dựa hoàn toàn vào Javascript Stack. Bạn muốn làm việc với nó, bạn phải có các kiến thức liên quan đến Javascript. _Nếu chưa có kinh nghiệm với Javascript, bạn có thể bắt đầu [ở đây](https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/JavaScript_basics) hoặc [Javascript – Ngôn ngữ lập trình của thời đại](https://nordiccoder.com/blog/javascript-ngon-ngu-lap-trinh-cua-thoi-dai/)_.

- Thứ hai, **Hot reloading, Live reload**: Nếu bạn đã làm việc với Android hoặc, mỗi lần mình thay đổi 1 xíu kiểu như một cái chuỗi để hiển thị, thình mình phải ngồi chờ nó rebuild lại project rồi mới run application trên device nó rất là mất nhiều thời gian. Với **Hot reloading**, giai đoạn build app khá nhanh và nếu mình có thay đổi gì, mình chỉ cần thay đổi và lưu lại, thì ứng dùng sẽ lập tức reload lại rất nhanh (_trường hợp bundle lần đầu tiên sẽ mất một ít thời gian_). Còn **live reload** nó sẽ load lại tất cả các file của Project. Nên chúng ta sẽ cân nhắc trước khi sử dụng 2 cái đó, nếu bạn làm với UI components, hot reloading là phù hợp cho làm việc với business logic.


## Làm thế nào để bắt đầu?
Nhu cần cần phát triển ứng dụng di động rất nhiều và developer cũng rất đa dạng với kiến thức nền tảng khác nhau.
### Nếu bạn là một Mobile developer!?
Bạn đã quen với mobile native, navigation, stack, UI thread, bạn chỉ cần tìm hiển thêm phần thiếu là Javascript và ES6, ReactJS và cách làm việc với API và state management của Web developement.

### Nếu bạn là một Web developer!?
Chắc chắn bạn phải biết về Javascript, nếu chưa có nhiều kinh nghiệm hãy bổ sung thêm kiến thức về nó đặt biệt là ES6, nếu không bạn hãy dừng lại. 

Nếu chưa quen thuộc với ReactJS, bạn hãy tìm hiểu ngay về ReactJS rồi hãy bắt đầu nhé. 

## Cài đặt những công cụ cần thiết
Nếu là một Developer, chắc chắn bạn phải cần có **git**. Bạn cần phải có cài đặt **Node JS** (version 8+), nếu chưa có bạn có thể [tải và làm theo hướng dẫn ở đây](https://nodejs.org/en/download/)  và **npm**. Có bạn sẽ tự hỏi có cần **yarn** không!? việc này tùy ở bạn, yarn không phải là bắt buộc.

Bạn cần cài thêm các máy ảo (**Simulator** cho iOS và **Emulator** cho Android) để phục vụ cho việc debug ứng dụng hoặc có thể dùng luôn điện thoại di động mình của mình cũng được.

## Vậy bắt đầu với React Native như thế nào?
Có 2 cách bắt đầu với, nếu bạn quen thuộc với mobile development, bạn đã có Xcode và Android Studio. Bạn nên bắt đầu với **Create React Native App**.

Một cách khác đơn giản hơn, Bạn có thể bắt đầu với Expo mà không cần thêm bất kỳ cài đặt nào khác như Xcode hoặc Android Studio.

Để bắt đầu, bạn chỉ cần cài đặt Expo CLI:
```bash
npm install -g expo-cli
```
Sau khi cài đặt expo-cli xong, bạn có thể kiểm tra expo có trong bash command hay chưa bằng cách: `expo --version`, nếu có kết quả thì xem như cài đặt thành công.

Để khởi tạo, bạn chỉ cần chạy lệnh:
```bash
expo init AwesomeProject
```
AwesomeProject là tên ứng dụng của bạn, `expo init` sẽ khỏi tạo folders, files và setting cần thiết.

Khi chạy lệnh này, bạn có 2 sự lựa chọn:
- **blank**: lựa chọn đơn giản nhất với ít packages nhất và 1 component duy nhất.
- **tabs**: lựa chọn phức tạp hơn với một số màn hình và template mẫu
  
Mình hãy lựa chọn **blank** nhé. Sau khi cài đặt thành công, bạn có thể nhanh chóng khỏi tạo một ứng dụng bằng Expo CLI command:

Sau lệnh này bạn sẽ có được folder như sau:

![folder structure](https://i.imgur.com/brAxa1E.png)

Sau đó, bạn chỉ cần chạy lệnh:
```bash
expo start
```
Bạn đã có một ứng dụng đơn giản nhất ở chế độ developement và một giao diện web (Metro Bundle) để quản lý, tương tác với Expo.
![metro bundle](https://i.imgur.com/A7w1WyQ.png)

Với giao diện này, bạn có thể scan QR code và chạy ứng dụng trên điện thoại của mình. Trên điện thoại của bạn nhớ cài Expo client vào nhé [Apple store](https://itunes.apple.com/app/apple-store/id982107779) hoặc [Google play](https://play.google.com/store/apps/details?id=host.exp.exponent&referrer=www).

Mình có Simulator trên mac nên có thể click lên menu `Run on iOS simulator`

![simulator](https://i.imgur.com/dsVVRsL.png)

Bạn có thể dễ dàng chia sẽ với với các thành viên khác trong nhóm của mình để chạy ứng dụng, trải nghiệm hoặc giúp bạn kiểm tra nhanh để phát hiện ra vấn đề sớm hơn. (Chú ý với thiết bị iOS, bạn phải tạo ra 1 tài khoản chung và các bạn khác trong nhóm cùng sử dụng 1 tài khoản.)

### React Native chạy như thế nào!?
Đâu tiên, khi bạn chạy lệnh `expo start` trong terminal, khi đó expo sẽ start với entry file là `App.js`:

```code
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
```

Nếu các bạn chưa quen với `import`, `export`, thì các bạn phải tự tìm hiểu thêm [ở đây](http://es6-features.org/#ValueExportImport) nhé.

```code
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
```
Vì React Native dựa vào React, nên việc `import` React từ react module là bắt buộc đối với các component muốn `extends` tù React Component trong React Native. Tiếp theo là `import` các components cơ bản của React Native như `View`, `Text` và `StyleSheet`.

Sau khi import xong:

```code
// import library

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
      </View>
    );
  }
}
```
Một Component của React khi extends từ `React.Component` phải có 1 method bắt buộc là `render`. Ở ví dụ trên, method này trả về 1 `View` and `Text` component.

Tiếp theo là Stylesheet:

```code
// import library

// implementation of component

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
```
`StyleSheet` là một `abstraction` tương tự như CSS Stylesheet khi làm việc với Web và Browser.

Tiếp theo thì bạn sẽ học thêm cách làm với các component, với các `View`, giống như `div` hoặc `Text` giống như `p` ở HTML. Sau đó sẽ là các component cần thiết khác như Image, Icon, Image background ,..vv

Tiếp theo bạn sẽ học cách sắp xếp và `làm đẹp` ứng dụng của mình tương tự như CSS ỏ Browser và dựa vào các thuộc tính cơ bản như color, backgroundColor, fontSize, padding và margin. Đặt biệt là không thể thiếu `flex` [Xem thêm và thực hành ở đây](https://facebook.github.io/react-native/docs/flexbox)

### Vậy `publish` ứng dụng như thế nào!?
Nếu bạn đăng ký tài khoản trên https://expo.io/, bạn có thể public ứng dụng của mình với chế độ build production như hình dưới:

![publish](https://i.imgur.com/eaZBQ10.png)

### Ứng dụng của mình cần nhiều hơn 1 màn hình, 1 component!?
Trên thực tế, ứng dụng của các bạn sẽ có nhiều màn hình (screens) khác nhau, bạn cần tìm hiểu cách làm việc với `react-navigation` để tổ chức các `navigator`, `stack` của mình. (Xêm thêm ở đây https://medium.com/@rossbulat/introduction-to-react-navigation-and-navigators-in-react-native-3efcf7239a43)

Có một số cách để làm navigation đó là:
- drawer menu
- tab bottom
- kết hợp drawer và tab

Ví dụ drawer menu:
![drawer](https://i.imgur.com/NlvJN2Y.png)


Ví dụ tab menu bao gồm 3 tabs:
![tab](https://i.imgur.com/7UT4Mol.png)

## Kết luận
Đây là một số bước để bạn bắt đầu một cách nhanh nhất với React Native. Một điều quan trọng bạn không thể bỏ qua đó là kiến thức về `Javascript` và `ReactJS` để có thể phát triển ứng dụng của mình một cách thuận lợi nhất.

Với Expo, bạn có thể làm thêm được nhiều thứ như lấy thông tin của thiết bị như tên, model, vv.... Bạn cũng có thể truy cập được thông tin như Contact, SMS hay Sensor hoặc push notification.

Trong một số trường hợp, Nếu bạn cần phát triển ứng dụng với Native component, Expo cho phép bạn `link` native code vào React Native nhé.

### Link tham khảo thêm:

- https://facebook.github.io/react-native/
- https://docs.expo.io/versions/latest/
- https://reactnavigation.org/
- https://medium.com/@rossbulat/introduction-to-react-navigation-and-navigators-in-react-native-3efcf7239a43

- https://nordiccoder.com/blog/nhung-uu-diem-noi-bat-cua-react-native/
- https://nordiccoder.com/blog/tong-quan-ve-react-native/
- https://nordiccoder.com/khoa-hoc-online/react-native-co-ban

